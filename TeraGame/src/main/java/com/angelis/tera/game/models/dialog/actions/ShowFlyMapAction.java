package com.angelis.tera.game.models.dialog.actions;

import com.angelis.tera.game.models.dialog.AbstractDialogAction;
import com.angelis.tera.game.models.player.Player;

public class ShowFlyMapAction extends AbstractDialogAction {

    public ShowFlyMapAction(Player player) {
        super(player);
    }

    @Override
    public void action() {

    }
}
