package com.angelis.tera.game.services;

import java.util.Map;

import javolution.util.FastMap;

import org.apache.log4j.Logger;

import com.angelis.tera.common.utils.Rnd;
import com.angelis.tera.game.models.TeraCreature;
import com.angelis.tera.game.models.creature.CreatureTemplate;
import com.angelis.tera.game.models.dialog.Dialog;
import com.angelis.tera.game.models.dialog.DialogButton;
import com.angelis.tera.game.models.dialog.actions.ShowBankAction;
import com.angelis.tera.game.models.dialog.actions.ShowFlyMapAction;
import com.angelis.tera.game.models.dialog.enums.DialogIconEnum;
import com.angelis.tera.game.models.dialog.enums.DialogStringEnum;
import com.angelis.tera.game.models.enums.CreatureTitleEnum;
import com.angelis.tera.game.models.player.Player;
import com.angelis.tera.game.models.player.enums.EmoteEnum;
import com.angelis.tera.game.network.connection.TeraGameConnection;
import com.angelis.tera.game.network.packet.server.SM_CREATURE_EMOTE;
import com.angelis.tera.game.network.packet.server.SM_PLAYER_DIALOG_CLOSE;
import com.angelis.tera.game.network.packet.server.SM_PLAYER_EMOTE;

public class DialogService extends AbstractService {

    /** LOGGER */
    private static Logger log = Logger.getLogger(DialogService.class.getName());
    
    /** INSTANCE */
    private static DialogService instance;
    
    private Map<Integer, Dialog> dialogs = new FastMap<>();
    
    @Override
    public void onInit() {
        log.info("DialogService started");
    }

    @Override
    public void onDestroy() {
        this.dialogs.clear();
        log.info("DialogService stopped");
    }
    
    public void onPlayerTalk(Player player, int creatureUId, int objectFamilly) {
        TeraCreature creature = SpawnService.getInstance().getCreatureByUid(creatureUId);
        if (creature == null) {
            return;
        }
        
        final CreatureTemplate creatureTemplate = creature.getCreatureTemplate();
        final Dialog dialog = new Dialog(player, creature, null, creatureTemplate.getHuntingZoneId() != 0 ? creatureTemplate.getHuntingZoneId() : 213);

        // TODO creature template
        final CreatureTitleEnum creatureTitle = creatureTemplate.getCreatureTitle();
        if (creatureTitle != null) {
            switch (creatureTitle) {
                case FLIGHT_MASTER:
                    dialog.getButtons().add(new DialogButton(DialogIconEnum.CENTERED_GRAY, DialogStringEnum.FLIGHTPOINTS, new ShowFlyMapAction(player)));
                break;
                
                case BANK:
                    dialog.getButtons().add(new DialogButton(DialogIconEnum.CENTERED_GRAY, DialogStringEnum.BANK, new ShowBankAction(player)));
                break;
            }
        }
        
        player.getController().setDialog(dialog);
        
        dialog.send();
        dialogs.put(dialog.getUid(), dialog);
    }
    
    public void onPlayerTalkProgress(Player player, int dialogUid, int choice) {
        final Dialog dialog = this.dialogs.get(dialogUid);
        if (dialog == null) {
            return;
        }
        
        dialog.progress(choice);
    }
    
    public void onPlayerMove(Player player) {
        final Dialog dialog = player.getController().getDialog();
        if (dialog == null) {
            return;
        }
        
        this.playerCloseDialog(player, dialog);
    }
    
    public void onDialogUpdate(Player player, int dialogUid, int action) {
        final Dialog dialog = this.dialogs.get(dialogUid);
        if (dialog == null) {
            return;
        }
        
        switch (action) {
            case 0:
                dialog.setEmotion(Rnd.get(1, 5, true));
                VisibleService.getInstance().sendPacketForVisible(player, new SM_CREATURE_EMOTE(dialog.getTeraCreature(), player, true, dialog.getEmotion()));
                VisibleService.getInstance().sendPacketForVisible(player, new SM_PLAYER_EMOTE(player, EmoteEnum.TALK, 0));
            break;
            
            case 2:
                this.playerCloseDialog(player, dialog);
            break;
        }
    }

    private final void playerCloseDialog(Player player, Dialog dialog) {
        final TeraGameConnection connection = player.getConnection();
        connection.sendPacket(new SM_PLAYER_DIALOG_CLOSE(dialog.getTeraCreature()));
        connection.sendPacket(new SM_CREATURE_EMOTE(dialog.getTeraCreature(), player, false, dialog.getEmotion()));
        player.getController().setDialog(null);
        
        this.dialogs.remove(dialog.getUid());
    }
    
    /** SINGLETON */
    public static DialogService getInstance() {
        if (instance == null) {
            instance = new DialogService();
        }
        return instance;
    }
}
