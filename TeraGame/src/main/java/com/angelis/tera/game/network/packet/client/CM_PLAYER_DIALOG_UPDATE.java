package com.angelis.tera.game.network.packet.client;

import java.nio.ByteBuffer;

import com.angelis.tera.game.network.connection.TeraGameConnection;
import com.angelis.tera.game.network.packet.TeraClientPacket;
import com.angelis.tera.game.services.DialogService;

public class CM_PLAYER_DIALOG_UPDATE extends TeraClientPacket {

    private int dialogUid;
    private int action;
    
    public CM_PLAYER_DIALOG_UPDATE(ByteBuffer byteBuffer, TeraGameConnection connection) {
        super(byteBuffer, connection);
    }

    @Override
    protected void readImpl() {
        this.dialogUid = readD(); // dialog id
        this.action = readD();
        readD(); // unk 0x00
    }

    @Override
    protected void runImpl() {
        DialogService.getInstance().onDialogUpdate(this.getConnection().getActivePlayer(), this.dialogUid, this.action);
    }
}
