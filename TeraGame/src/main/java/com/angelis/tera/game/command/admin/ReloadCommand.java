package com.angelis.tera.game.command.admin;

import com.angelis.tera.common.utils.Function;
import com.angelis.tera.game.models.player.Player;
import com.angelis.tera.game.network.connection.TeraGameConnection;
import com.angelis.tera.game.network.packet.ClientPacketHandler;
import com.angelis.tera.game.network.packet.ServerPacketHandler;
import com.angelis.tera.game.services.SpawnService;
import com.angelis.tera.game.services.VisibleService;
import com.angelis.tera.game.services.WorldService;
import com.angelis.tera.game.services.XMLService;

public class ReloadCommand extends AbstractAdminCommand {

    @Override
    public void execute(TeraGameConnection connection, String[] arguments) {
        switch (arguments[0]) {
            case "network":
                ServerPacketHandler.init();
                ClientPacketHandler.init();
            break;
            
            case "xml":
                SpawnService.getInstance().stop();
                WorldService.getInstance().doOnAllOnlinePlayer(new Function<Player>() {
                    @Override
                    public void call(Player player) {
                        VisibleService.getInstance().updatePlayerObservers(player);
                        VisibleService.getInstance().updatePlayerObservables(player);
                    }
                });
                
                SpawnService.getInstance().start();
                XMLService.getInstance().restart();
                WorldService.getInstance().doOnAllOnlinePlayer(new Function<Player>() {
                    @Override
                    public void call(Player player) {
                        VisibleService.getInstance().updatePlayerObservers(player);
                        VisibleService.getInstance().updatePlayerObservables(player);
                    }
                });
            break;
        }
    }

    @Override
    public int getAccessLevel() {
        return 1;
    }

    @Override
    public int getArgumentCount() {
        return 1;
    }

    @Override
    public boolean checkArguments(String[] arguments) {
        return true;
    }

    @Override
    public String getSyntax() {
        return "Syntax: !reload {network}";
    }
}
