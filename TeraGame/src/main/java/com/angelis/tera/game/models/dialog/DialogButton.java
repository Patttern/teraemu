package com.angelis.tera.game.models.dialog;

import com.angelis.tera.game.models.dialog.enums.DialogIconEnum;
import com.angelis.tera.game.models.dialog.enums.DialogQuestEnum;
import com.angelis.tera.game.models.dialog.enums.DialogStringEnum;

public class DialogButton {
    public final DialogIconEnum dialogIcon;
    public final String text;
    public final AbstractDialogAction dialogAction;
    
    public DialogButton(DialogIconEnum dialogIcon, String text, AbstractDialogAction dialogAction) {
        this.dialogIcon = dialogIcon;
        this.text = text;
        this.dialogAction = dialogAction;
    }
    
    public DialogButton(DialogIconEnum dialogIcon, DialogStringEnum dialogString, AbstractDialogAction dialogAction) {
        this.dialogIcon = dialogIcon;
        this.text = "@npc:"+dialogString.value;
        this.dialogAction = dialogAction;
    }

    public DialogButton(DialogIconEnum dialogIcon, DialogQuestEnum dialogQuest, AbstractDialogAction dialogAction) {
        this.dialogIcon = dialogIcon;
        this.text = "@quest:"+dialogQuest.value;
        this.dialogAction = dialogAction;
    }
    
    public final void action() {
        this.dialogAction.action();
    }

    public DialogIconEnum getDialogIcon() {
        return dialogIcon;
    }

    public String getText() {
        return text;
    }
}
