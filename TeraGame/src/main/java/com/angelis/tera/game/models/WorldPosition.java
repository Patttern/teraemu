package com.angelis.tera.game.models;

import com.angelis.tera.common.utils.Point3D;

public final class WorldPosition implements Cloneable {
    
    private int mapId;
    private Point3D point3D;
    private short heading;
    
    public WorldPosition() {
        super();
        this.point3D = new Point3D();
    }
    
    public WorldPosition(int mapId, float x, float y, float z) {
        super();
        this.mapId = mapId;
        this.point3D = new Point3D(x, y, z);
    }
    
    public WorldPosition(int mapId, float x, float y, float z, short heading) {
        this(mapId, x, y, z);
        this.heading = heading;
    }
    
    public double distanceTo(float x, float y) {
        double a = x - this.getX();
        double b = y - this.getY();

        return Math.sqrt(a*a + b*b);
    }
    
    public double distanceTo(float x, float y, float z) {
        double a = x - this.getX();
        double b = y - this.getY();
        double c = z - this.getZ();

        return Math.sqrt(a*a + b*b + c*c);
    }
    
    public double distanceTo(WorldPosition otherWorldPosition) {
        if (otherWorldPosition == null) {
            return Double.MAX_VALUE;
        }
        
        return this.distanceTo(otherWorldPosition.getX(), otherWorldPosition.getY(), otherWorldPosition.getZ());
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public Point3D getPoint3D() {
        return point3D;
    }

    public void setPoint3D(Point3D point3d) {
        point3D = point3d;
    }

    public float getX() {
        return this.point3D.getX();
    }

    public void setX(float x) {
        this.point3D.setX(x);
    }
    
    public void addX(int x) {
        this.setX(this.getX()+x);
    }

    public float getY() {
        return point3D.getY();
    }

    public void setY(float y) {
        point3D.setY(y);
    }
    
    public void addY(int y) {
        this.setY(this.getY()+y);
    }

    public float getZ() {
        return point3D.getZ();
    }

    public void setZ(float z) {
        point3D.setZ(z);
    }
    
    public void addZ(int z) {
        this.setZ(this.getZ()+z);
    }

    public void setXYZ(float x, float y, float z) {
        this.setX(x);
        this.setY(y);
        this.setZ(z);
    }

    public short getHeading() {
        return heading;
    }

    public void setHeading(short heading) {
        this.heading = heading;
    }

    @Override
    public WorldPosition clone() {
        return new WorldPosition(this.mapId, this.getX(), this.getY(), this.getZ());
    }
}
