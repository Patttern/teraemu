package com.angelis.tera.game.controllers;

import java.util.EnumSet;

import com.angelis.tera.game.controllers.enums.RightEnum;
import com.angelis.tera.game.models.chainedaction.AbstractChainedAction;
import com.angelis.tera.game.models.dialog.Dialog;
import com.angelis.tera.game.models.player.Player;
import com.angelis.tera.game.models.player.request.Request;

public class PlayerController extends Controller<Player> {

    private final EnumSet<RightEnum> rights = EnumSet.of(RightEnum.WALK, RightEnum.TALK);
    
    private Request request;
    private Dialog dialog;
    private AbstractChainedAction chaineAction;
    
    public boolean can(RightEnum right) {
        return this.rights.contains(right);
    }

    public final void removeRight(RightEnum right) {
        this.rights.remove(right);
    }
    
    public final void addRight(RightEnum right) {
        this.rights.add(right);
    }

    public Request getRequest() {
        return request;
    }
    
    public void setRequest(Request request) {
        this.request = request;
    }

    public Dialog getDialog() {
        return dialog;
    }
    
    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public AbstractChainedAction getChaineAction() {
        return chaineAction;
    }

    public void setChaineAction(AbstractChainedAction chaineAction) {
        this.chaineAction = chaineAction;
    }
}
