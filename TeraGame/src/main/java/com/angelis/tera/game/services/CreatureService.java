package com.angelis.tera.game.services;

import org.apache.log4j.Logger;

import com.angelis.tera.game.models.Creature;
import com.angelis.tera.game.models.CreatureEventTypeEnum;
import com.angelis.tera.game.models.WorldPosition;
import com.angelis.tera.game.utils.Geom;

public class CreatureService extends AbstractService {

    /** LOGGER */
    private static Logger log = Logger.getLogger(CreatureService.class.getName());
    
    /** INSTANCE */
    private static CreatureService instance;
    
    @Override
    public void onInit() {
        log.info("CreatureService started");
    }

    @Override
    public void onDestroy() {
        log.info("CreatureService stopped");
    }

    public void moveCreature(Creature creature, WorldPosition newWorldPosition) {
        WorldPosition worldPosition = creature.getWorldPosition();
        short heading = Geom.getHeading(worldPosition, newWorldPosition);
        creature.notifyObservers(CreatureEventTypeEnum.MOVE, newWorldPosition.getX(), newWorldPosition.getY(), newWorldPosition.getZ(), heading, worldPosition.getX(), worldPosition.getY(), worldPosition.getZ());
        
        worldPosition.setPoint3D(newWorldPosition.getPoint3D());
        worldPosition.setHeading(heading);
    }
    
    /** SINGLETON */
    public static CreatureService getInstance() {
        if (instance == null) {
            instance = new CreatureService();
        }
        return instance;
    }
}
