package com.angelis.tera.game.ai;

import com.angelis.tera.game.models.Creature;

public abstract class AI extends Thread {
    
    protected final Creature creature;
    
    public AI(Creature creature) {
        this.creature = creature;
    }

    public final void run() {
        while (true) {
            this.runImpl();
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    public abstract void runImpl();
}
