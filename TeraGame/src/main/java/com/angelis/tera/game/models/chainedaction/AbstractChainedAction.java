package com.angelis.tera.game.models.chainedaction;

public abstract class AbstractChainedAction {
    
    public abstract void start();
    public abstract void stop();
    
    public abstract void next();
}
