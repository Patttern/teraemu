package com.angelis.tera.game.ai;

import com.angelis.tera.common.utils.CircularArrayList;
import com.angelis.tera.common.utils.Point3D;
import com.angelis.tera.game.models.TeraCreature;
import com.angelis.tera.game.models.WorldPosition;
import com.angelis.tera.game.services.CreatureService;

public class PathAI extends AI {
    
    private final CircularArrayList<Point3D> paths;
    private Point3D current;
    private int vector = 5; // TODO
    
    public PathAI(TeraCreature teraCreature, CircularArrayList<Point3D> paths) {
        super(teraCreature);
        this.paths = paths;
    }

    @Override
    public void runImpl() {
        if (current == null) {
            current = this.paths.first();
        }
        
        final WorldPosition worldPosition = this.creature.getWorldPosition().clone();
        double distanceX = worldPosition.getX()-this.current.getX();
        double distanceY = worldPosition.getY()-this.current.getY();
        double distanceZ = worldPosition.getZ()-this.current.getZ();
        
        if (distanceX == 0 && distanceY == 0 && distanceZ == 0) {
            current = this.paths.next();
            return;
        }
        
        if (distanceX < 0) {
            worldPosition.addX(-this.vector);
        } else {
            worldPosition.addX(this.vector);
        }
        
        if (distanceY < 0) {
            worldPosition.addY(-this.vector);
        } else {
            worldPosition.addY(this.vector);
        }
        
        if (distanceZ < 0) {
            worldPosition.addZ(-this.vector);
        } else {
            worldPosition.addZ(this.vector);
        }
        
        CreatureService.getInstance().moveCreature(this.creature, worldPosition);
    }
}
