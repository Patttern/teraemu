package com.angelis.tera.game.models.dialog;

import com.angelis.tera.game.models.player.Player;

public abstract class AbstractDialogAction {
    
    protected final Player player;
    
    public AbstractDialogAction(final Player player) {
        this.player = player;
    }
    
    public abstract void action();
}
