package com.angelis.tera.game.network.packet.server;

import java.nio.ByteBuffer;

import com.angelis.tera.game.models.Creature;
import com.angelis.tera.game.models.TeraCreature;
import com.angelis.tera.game.network.connection.TeraGameConnection;
import com.angelis.tera.game.network.packet.TeraServerPacket;

public class SM_CREATURE_EMOTE extends TeraServerPacket {

    private final TeraCreature teraCreature;
    private final Creature target;
    private final boolean start;
    private final int emotion;
    
    public SM_CREATURE_EMOTE(TeraCreature teraCreature, Creature target, boolean start, int emotion) {
        this.teraCreature = teraCreature;
        this.target = target;
        this.start = start;
        this.emotion = emotion;
    }

    @Override
    protected void writeImpl(TeraGameConnection connection, ByteBuffer byteBuffer) {
        writeUid(byteBuffer, this.teraCreature);
        writeUid(byteBuffer, this.target);

        writeD(byteBuffer, start ? 0 : 2); // this can be 0, 1 or 2
        writeD(byteBuffer, emotion); // this can be 0, 1, 2, 3, 4 or 5
        writeD(byteBuffer, 0);
    }
}
