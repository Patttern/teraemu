package com.angelis.tera.game.utils;

import com.angelis.tera.game.models.WorldPosition;

public class Geom {
    public static short getHeading(WorldPosition fromWorldPosition, WorldPosition toWorldPosition) {
        return (short) (Math.atan2(toWorldPosition.getY() - fromWorldPosition.getY(), toWorldPosition.getX() - fromWorldPosition.getX()) * 32768 / Math.PI);
    }
}
